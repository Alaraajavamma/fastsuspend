# FastSuspend for mobile-linux 

This is simple script which will monitor:
1. If screen is off - proceed to 2
2. If there are no inhibitors proceed to 3
3. If there are no active calls proceed to 4
4. Suspend

Currently this works only on original Pinephone (not Pro) but it should be quite easy to port it for Librem 5 and Pro also. I will try to find time to do it.


## How to install?

`cd ~ && mkdir -p .git && cd .git && git clone https://gitlab.com/Alaraajavamma/fastsuspend && cd fastsuspend && sudo chmod +x install.sh && ./install.sh `

And reboot and it should just work

Uninstall

`cd ~ && cd .git && rm -rf fastsuspend && rm ~/.confit/autostart/fastsuspend.desktop && sudo rm -rf /opt/fastsuspend `

## License
Feel free to do what ever you want with this but no guarantees - this will probably explode your phone xD

## Something else?
If you wan't to help or find issue feel free to contact

