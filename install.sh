#!/bin/bash

if [ "$(id -u)" -eq 0 ]; then
    echo "Script must not be ran as root"
    exit 1
fi

echo "This FastSuspend fork is ment to run in systemd based operating system - it does not work without"

sudo chmod +x ./*.sh
mkdir -p "${HOME}/.config/autostart/"
sudo mkdir -p /opt/fastsuspend/
sudo ln -s "${PWD}/install.sh" "/opt/fastsuspend/"
sudo ln -s "${PWD}/fastsuspend.sh" "/opt/fastsuspend/"
sudo ln -s "${PWD}/README.md" "/opt/fastsuspend/"
sudo ln -s "${PWD}/fastsuspend.svg" "/opt/fastsuspend/"
ln -s "${PWD}/fastsuspend.desktop" "${HOME}/.config/autostart/fastsuspend.desktop"
