#!/bin/bash

function check_conditions() {
    condition1=$(cat /sys/class/backlight/backlight/bl_power)
    condition2=$(gnome-session-inhibit -l)
    condition3=$(mmcli -m any --voice-list-calls)

    if [[ $condition1 != "0" ]] && [[ $condition2 == "No inhibitors" ]] && [[ $condition3 == "No calls were found" ]]; then
        return 0
    else
        return 1
    fi
}

function turn_on_off_leds() {
        echo 0 > /sys/class/leds/blue\:indicator/brightness
        echo 0 > /sys/class/leds/red\:indicator/brightness
        echo 1 > /sys/class/leds/green\:indicator/brightness
        sleep $sec
        echo 0 > /sys/class/leds/blue\:indicator/brightness
        echo 0 > /sys/class/leds/red\:indicator/brightness
        echo 0 > /sys/class/leds/green\:indicator/brightness
        sleep $sec
        echo 0 > /sys/class/leds/blue\:indicator/brightness
        echo 0 > /sys/class/leds/red\:indicator/brightness
        echo 1 > /sys/class/leds/green\:indicator/brightness
        sleep $sec
        echo 0 > /sys/class/leds/blue\:indicator/brightness
        echo 0 > /sys/class/leds/red\:indicator/brightness
        echo 0 > /sys/class/leds/green\:indicator/brightness
}

while true; do
    if check_conditions; then
       sec=0.2
       turn_on_off_leds
    if check_conditions; then
       sec=0.19
       turn_on_off_leds
    if check_conditions; then
       sec=0.18
       turn_on_off_leds
    if check_conditions; then
       sec=0.17
       turn_on_off_leds
    if check_conditions; then
       sec=0.16
       turn_on_off_leds
    if check_conditions; then
       sec=0.15
       turn_on_off_leds
    if check_conditions; then
       sec=0.14
       turn_on_off_leds
    if check_conditions; then
       sec=0.13
       turn_on_off_leds
    if check_conditions; then
       sec=0.12
       turn_on_off_leds
    if check_conditions; then
        sec=0.11
        turn_on_off_leds	
    if check_conditions; then
       sec=0.1
       turn_on_off_leds
    if check_conditions; then
       sec=0.09
       turn_on_off_leds
    if check_conditions; then
       sec=0.08
       turn_on_off_leds
    if check_conditions; then
       sec=0.07
       turn_on_off_leds
    if check_conditions; then
       sec=0.06
       turn_on_off_leds
    if check_conditions; then
       sec=0.05
       turn_on_off_leds
    if check_conditions; then
       sec=0.04
       turn_on_off_leds
    if check_conditions; then
       sec=0.03
       turn_on_off_leds
    if check_conditions; then
       sec=0.02
       turn_on_off_leds
    if check_conditions; then
        sec=0.01
        turn_on_off_leds
    if check_conditions; then
        systemctl suspend
        sleep 3
        else
        sleep 3
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
    fi
done
